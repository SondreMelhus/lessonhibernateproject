package com.example.lessonhibernate.exceptions;

public class UpdateException extends Exception {
    public UpdateException (String message) { super(message); }
}
