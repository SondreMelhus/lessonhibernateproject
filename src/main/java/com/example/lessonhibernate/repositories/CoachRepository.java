package com.example.lessonhibernate.repositories;

import com.example.lessonhibernate.models.Coach;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CoachRepository extends JpaRepository<Coach, Integer> {
}
