package com.example.lessonhibernate.repositories;

import com.example.lessonhibernate.models.Athlete;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AthleteRepository extends JpaRepository<Athlete, Integer> {
}
