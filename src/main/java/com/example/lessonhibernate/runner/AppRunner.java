package com.example.lessonhibernate.runner;

import com.example.lessonhibernate.services.athlete.AthleteService;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class AppRunner implements ApplicationRunner {

    private final AthleteService athleteService;


    public AppRunner(AthleteService athleteService) {
        this.athleteService = athleteService;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        athleteService.updateCoach(8,4);
    }
}
