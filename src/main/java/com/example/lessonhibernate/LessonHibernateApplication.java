package com.example.lessonhibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LessonHibernateApplication {

    public static void main(String[] args) {
        SpringApplication.run(LessonHibernateApplication.class, args);
    }

}
