package com.example.lessonhibernate.services.coach;

import com.example.lessonhibernate.models.Coach;
import com.example.lessonhibernate.services.coach.CoachService;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class CoachServiceImpl implements CoachService {
    @Override
    public Coach findById(Integer integer) {
        return null;
    }

    @Override
    public Collection<Coach> findAll() {
        return null;
    }

    @Override
    public Coach add(Coach entity) {
        return null;
    }

    @Override
    public Coach update(Coach entity) {
        return null;
    }

    @Override
    public void deleteById(Integer integer) {

    }

    @Override
    public void delete(Coach entity) {

    }
}
