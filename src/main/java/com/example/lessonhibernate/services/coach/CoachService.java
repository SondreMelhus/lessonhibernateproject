package com.example.lessonhibernate.services.coach;

import com.example.lessonhibernate.models.Coach;
import com.example.lessonhibernate.services.CrudService;
import org.springframework.stereotype.Service;

@Service
public interface CoachService extends CrudService<Coach, Integer> {
}
