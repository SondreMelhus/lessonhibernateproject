package com.example.lessonhibernate.services.athlete;

import com.example.lessonhibernate.models.Athlete;
import com.example.lessonhibernate.repositories.AthleteRepository;
import com.example.lessonhibernate.repositories.CoachRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;

@Service
public class AthleteServiceImpl implements AthleteService {

    private final AthleteRepository athleteRepository;
    private final CoachRepository coachRepository;
    private final Logger logger = LoggerFactory.getLogger(AthleteServiceImpl.class);

    public AthleteServiceImpl(AthleteRepository athleteRepository, CoachRepository coachRepository) {
        this.athleteRepository = athleteRepository;
        this.coachRepository = coachRepository;
    }


    @Override
    @Transactional
    public void updateCoach(Integer athleteId, Integer coachId) {
        if (coachRepository.existsById(coachId)) {
            Athlete athlete = athleteRepository.findById(athleteId).get();
            athlete.setCoach(coachRepository.findById(coachId).get());
            athleteRepository.save(athlete);
        } else {
            logger.warn("There is no coach in the database with the id: " + coachId);
        }
    }

    @Override
    public Athlete findById(Integer integer) {
        if (athleteRepository.existsById(integer)) {
            return athleteRepository.findById(integer).get();
        }
        return null;
    }

    @Override
    public Collection<Athlete> findAll() {
        return athleteRepository.findAll();
    }

    @Override
    public Athlete add(Athlete entity) {
        return athleteRepository.save(entity);
    }

    @Override
    public Athlete update(Athlete entity) {
        return athleteRepository.save(entity);
    }

    @Override
    public void deleteById(Integer integer) {
        athleteRepository.deleteById(integer);
    }

    @Override
    public void delete(Athlete entity) {
        athleteRepository.delete(entity);
    }
}
