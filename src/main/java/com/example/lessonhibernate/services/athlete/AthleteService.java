package com.example.lessonhibernate.services.athlete;

import com.example.lessonhibernate.models.Athlete;
import com.example.lessonhibernate.services.CrudService;
import org.springframework.stereotype.Service;

@Service
public interface AthleteService extends CrudService<Athlete, Integer> {
    void updateCoach(Integer athleteId, Integer coachId);
}
