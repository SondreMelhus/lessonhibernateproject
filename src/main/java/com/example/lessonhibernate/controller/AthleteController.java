package com.example.lessonhibernate.controller;

import com.example.lessonhibernate.models.Athlete;
import com.example.lessonhibernate.services.athlete.AthleteService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/athletes")
public class AthleteController {

    private final AthleteService athleteService;

    public AthleteController(AthleteService athleteService) {
        this.athleteService = athleteService;
    }

    @GetMapping("{id}")
    public ResponseEntity<Athlete> findById(@PathVariable int id) {
        return ResponseEntity.ok(athleteService.findById(id));
    }

    //findAll
    @GetMapping
    public ResponseEntity<Collection<Athlete>> findAll() {
        return ResponseEntity.ok(athleteService.findAll());
    }

    //add
    @PostMapping
    public ResponseEntity add(@RequestBody Athlete athlete) {
        Athlete  ath = athleteService.add(athlete);
        URI location = URI.create("athletes/" + ath.getId());
        return ResponseEntity.created(location).build();
    }

    //update
    @PostMapping("{id}")
    public ResponseEntity update(@RequestBody Athlete athlete, @PathVariable int id) {

        if (id != athlete.getId())
            return ResponseEntity.badRequest().build();

        athleteService.update(athlete);
        return ResponseEntity.noContent().build();
    }


    //deleteById
    @DeleteMapping("{id}")
    public ResponseEntity deleteById(@PathVariable int id) {
        athleteService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    //delete
    @DeleteMapping
    public ResponseEntity delete(@RequestBody Athlete athlete) {

        if (athleteService.findById(athlete.getId()) == null)
            return ResponseEntity.badRequest().build();

        athleteService.delete(athlete);
        return ResponseEntity.noContent().build();
    }
}
