package com.example.lessonhibernate.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
public class Coach {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;
    @Column (length = 50, nullable = false)
    private String name;
    @OneToMany(mappedBy = "coach")
    private Set<Athlete> athletes;
    /*
    @ManyToMany
    @JoinTable (
            name = "coach_certificate",
            joinColumns = {@JoinColumn(name = "coach_id")},
            inverseJoinColumns = {@JoinColumn(name = "certificate_id")}
    )
    private Set<Certificate> certificates;
     */
}
