package com.example.lessonhibernate.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class Certificate {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;
    @Column (length = 6, nullable = false)
    private String code;
    @Column (length = 50, nullable = false)
    private String title;
    /*
    @ManyToMany (mappedBy = "certificates")
    private Set<Coach> coaches;

     */
}
