package com.example.lessonhibernate.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Data
@Entity
public class Athlete {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;
    @Column (length = 50, nullable = false)
    private String name;
    @ManyToOne
    @JoinColumn(name = "coach_id")
    private Coach coach;
    @JsonGetter("coach")
    public Integer jsonGetCoach() {
        if (coach != null)
            return coach.getId();
        return null;
    }
    /*
    @ManyToMany (mappedBy = "athletes")
    private Set<Event> events;
     */
}
