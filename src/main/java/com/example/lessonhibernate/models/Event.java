package com.example.lessonhibernate.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
public class Event {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;
    @Column (length = 50, nullable = false)
    private String name;
    /*
    @ManyToMany
    private Set<Athlete> athletes;
     */
}
