INSERT INTO event ("name") VALUES ('Mario Kart party');
INSERT INTO event ("name") VALUES ('F1');


INSERT INTO coach ("name") VALUES ('William Fences'); -- 1
INSERT INTO coach ("name") VALUES ('Arvid Strøm'); -- 2
INSERT INTO coach ("name") VALUES ('Anne Hansen'); -- 3
INSERT INTO coach ("name") VALUES ('Liv Larsen'); -- 4
-- Students
INSERT INTO athlete ("name", coach_id) VALUES ('Ola Nordmann', 1); -- 1
INSERT INTO athlete ("name", coach_id) VALUES ('Emma Hansen', 1); -- 2
INSERT INTO athlete ("name", coach_id) VALUES ('Olivia Nordmann', 2); -- 3
INSERT INTO athlete ("name", coach_id) VALUES ('Lucas Olsen', 2); -- 4
INSERT INTO athlete ("name", coach_id) VALUES ('Askel Nilsen', 3); -- 5
INSERT INTO athlete ("name", coach_id) VALUES ('Frida Kristiansen', 3); -- 6
INSERT INTO athlete ("name", coach_id) VALUES ('Ingrid Johansen', 4); -- 7
INSERT INTO athlete ("name", coach_id) VALUES ('Jakob Andersen', 4); -- 8

/*
INSERT INTO certificates (code, title) VALUES ('MPM', 'Mario Party Master');

INSERT INTO coach_certificates (coach_id, certificates_id) VALUES (1,1);
INSERT INTO coach_certificates (coach_id, certificates_id) VALUES (2,1);
INSERT INTO coach_certificates (coach_id, certificates_id) VALUES (3,1);
*/